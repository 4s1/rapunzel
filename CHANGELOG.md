# Changelog

## Unreleased

## 0.2.0 (2025-02-12)

- chore: update from Nest.js v10 to v11
- chore: update corepack from v0.30.0 to latest (currently v0.31.0)

## 0.1.0 (2025-01-27)

- fuel prices support
- stock prices support
- cache API
- Ntfy service
