import { CACHE_MANAGER } from '@nestjs/cache-manager';
import {
  Controller,
  Get,
  Post,
  HttpException,
  HttpStatus,
  Inject,
  Logger,
  Param,
  Body,
} from '@nestjs/common';
import { Cache } from 'cache-manager';

@Controller('caches')
export class CachesController {
  private readonly logger = new Logger(CachesController.name, {
    timestamp: true,
  });

  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  @Get(':name')
  async get(@Param('name') name: string): Promise<any> {
    this.logger.log(`Try to load cache ${name}`);

    const cache = await this.cacheManager.get<string>(`caches_${name}`);

    if (!cache) {
      this.logger.warn(`Cache ${name} not initialized`);
      throw new HttpException(
        'Cache not initialized',
        HttpStatus.FAILED_DEPENDENCY,
      );
    }

    return cache;
  }

  @Post(':name')
  async post(@Param('name') name: string, @Body() data: string): Promise<void> {
    this.logger.log(`Try to save cache ${name}`);

    await this.cacheManager.set(`caches_${name}`, data, 999999999);
  }
}
