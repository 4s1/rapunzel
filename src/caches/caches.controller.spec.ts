import { Test, TestingModule } from '@nestjs/testing';
import { CachesController } from './caches.controller';
import { CACHE_MANAGER } from '@nestjs/cache-manager';

describe('CachesController', () => {
  let controller: CachesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CachesController],
      providers: [
        {
          provide: CACHE_MANAGER,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<CachesController>(CachesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
