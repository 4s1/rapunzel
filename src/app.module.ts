import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from './config/configuration';
import { ConfigModule } from '@nestjs/config';
import { FuelPricesController } from './fuel-prices/fuel-prices.controller';
import { StockPricesController } from './stock-prices/stock-prices.controller';
import { CacheModule } from '@nestjs/cache-manager';
import { NtfyService } from './services/ntfy/ntfy.service';
import { CachesController } from './caches/caches.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
    CacheModule.register(),
  ],
  controllers: [
    AppController,
    FuelPricesController,
    StockPricesController,
    CachesController,
  ],
  providers: [AppService, NtfyService],
})
export class AppModule {}
