import { Controller, Get, Param, Logger, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { StockPricesConfig } from 'src/config/configuration';
import * as cheerio from 'cheerio';
import { DateTime } from 'luxon';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

// ToDo: Move to service
function isLetter(str: string): boolean {
  return str.length === 1 && 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.includes(str);
}

// ToDo: Move to service
export function isIsin(isin: unknown): isin is ISIN {
  if (!(typeof isin === 'string')) {
    return false;
  }
  return (
    isin.length === 12 && isLetter(isin.charAt(0)) && isLetter(isin.charAt(1))
  );
}

// ToDo: Move to service
type Letter =
  | 'A'
  | 'B'
  | 'C'
  | 'D'
  | 'E'
  | 'F'
  | 'G'
  | 'H'
  | 'I'
  | 'J'
  | 'K'
  | 'L'
  | 'M'
  | 'N'
  | 'O'
  | 'P'
  | 'Q'
  | 'R'
  | 'S'
  | 'T'
  | 'U'
  | 'V'
  | 'W'
  | 'X'
  | 'Y'
  | 'Z';

export type ISIN = `${Letter}${Letter}${string}`;

type Result = {
  timestamp: number;
  dateTime: string;
  last: number;
  high: number;
  low: number;
  isin: string;
} | null;

@Controller('stock-prices')
export class StockPricesController {
  private readonly logger = new Logger(StockPricesController.name, {
    timestamp: true,
  });

  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private configService: ConfigService,
  ) {}

  @Get(':isin')
  async findAll(@Param('isin') isin: string): Promise<Result> {
    const config = this.configService.get<StockPricesConfig>('stockPrices');

    if (!config) {
      // ToDo: Error code
      throw new Error();
    }

    this.logger.log(`request isin ${isin}`);

    if (!isIsin(isin)) {
      // ToDo: Error code
      throw new Error('Not an ISIN');
    }

    const cachedIsinDataString = await this.cacheManager.get<string>(isin);

    let data: Result = null;

    if (!cachedIsinDataString) {
      this.logger.log(`Load ISIN ${isin} from remote url`);
      const url = config.url.replace('$isin', isin);

      this.logger.log(url);

      const response = await fetch(url);
      const dom = cheerio.load(await response.text());
      const dt = DateTime.now();
      const timestamp = dt.toMillis();
      const dateTime = dt.toFormat('yyyy-HH-dd HH:mm:ss');
      const last = +dom(`#${config.selector.last}`)
        .text()
        .replace(',', '.')
        .replace(' ', '');
      const high = +dom(`#${config.selector.high}`)
        .text()
        .replace(',', '.')
        .replace(' ', '');
      const low = +dom(`#${config.selector.low}`)
        .text()
        .replace(',', '.')
        .replace(' ', '');

      this.logger.log(`last of ${isin} from ${dateTime} is ${last}`);

      data = {
        timestamp,
        dateTime,
        last,
        high,
        low,
        isin,
      };

      await this.cacheManager.set(isin, JSON.stringify(data), 1000 * 60 * 60);
    } else {
      this.logger.log(`Load ISIN ${isin} from cache`);
      data = JSON.parse(cachedIsinDataString) as Result;
    }

    return data;
  }
}
