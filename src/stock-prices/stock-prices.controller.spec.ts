import { Test, TestingModule } from '@nestjs/testing';
import { StockPricesController } from './stock-prices.controller';
import { ConfigService } from '@nestjs/config';
import { CACHE_MANAGER } from '@nestjs/cache-manager';

describe('StockPricesController', () => {
  let controller: StockPricesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockPricesController],
      providers: [
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              return '_' + key;
            }),
          },
        },
        {
          provide: CACHE_MANAGER,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<StockPricesController>(StockPricesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
