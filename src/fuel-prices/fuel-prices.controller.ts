import { Controller, Get, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FuelPricesConfig } from 'src/config/configuration';

interface TankerkoenigPricesRemoteResponse {
  ok: true;
  license: string;
  data: string;
  prices: TankerkoenigPriceMeta;
}

interface TankerkoenigPrice {
  status: 'open' | 'closed' | 'no prices';
  e5?: number | false;
  e10?: number | false;
  diesel?: number | false;
}

type TankerkoenigPriceMeta = Record<string, TankerkoenigPrice>;

@Controller('fuel-prices')
export class FuelPricesController {
  private readonly logger = new Logger(FuelPricesController.name, {
    timestamp: true,
  });

  constructor(private configService: ConfigService) {}

  @Get()
  async findAll(): Promise<TankerkoenigPricesRemoteResponse> {
    const config = this.configService.get<FuelPricesConfig>('fuelPrices');

    if (!config) {
      // ToDo: Error code
      throw new Error();
    }

    // ToDo: Check if fuelPrices are enabled

    // // ToDo: move this pattern to helper function
    // if (fastify.state.fuelPrices.lastRequest + 5 * 60 * 1000 >= Date.now()) {
    //     this.logger.warn("[fuel-prices] send cached result");
    //     return fastify.state.fuelPrices.lastResult;
    //   }
    //   fastify.state.fuelPrices.lastRequest = Date.now();
    this.logger.log('send fresh result');

    const stationIds = config.groups.map((group) =>
      group.stations.map((station) => station.id),
    );

    // Limit gas station count -> tankerkönig supports only 10 per request
    if (stationIds.length > 10) {
      stationIds.length = 10;
    }

    const urlParams = [
      `apikey=${config.apiKey}`,
      `ids=${stationIds.join(',')}`,
    ];

    const url = `https://creativecommons.tankerkoenig.de/json/prices.php?${urlParams.join('&')}`;
    const response = await fetch(url);
    const data = (await response.json()) as TankerkoenigPricesRemoteResponse;
    // ToDo: Parse result
    // ToDo: Group result
    //   fastify.state.fuelPrices.lastResult = data;

    return data;
  }
}
