import { Test, TestingModule } from '@nestjs/testing';
import { FuelPricesController } from './fuel-prices.controller';
import { ConfigService } from '@nestjs/config';

describe('FuelPricesController', () => {
  let controller: FuelPricesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FuelPricesController],
      providers: [
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              return '_' + key;
            }),
          },
        },
      ],
    }).compile();

    controller = module.get<FuelPricesController>(FuelPricesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
