import { Test, TestingModule } from '@nestjs/testing';
import { NtfyService } from './ntfy.service';
import { ConfigService } from '@nestjs/config';

describe('NtfyService', () => {
  let service: NtfyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              return '_' + key;
            }),
          },
        },
        NtfyService,
      ],
    }).compile();

    service = module.get<NtfyService>(NtfyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
