import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NtfyConfig } from 'src/config/configuration';

@Injectable()
export class NtfyService {
  private readonly logger = new Logger(NtfyService.name, {
    timestamp: true,
  });
  constructor(private configService: ConfigService) {}

  async send(topic: string, msg: string) {
    const config = this.configService.get<NtfyConfig>('ntfy');

    if (!config) {
      // ToDo: Error code
      throw new Error();
    }

    this.logger.log(`Send ntfy msg to topic ${topic}`);

    await fetch(`${config.url}/${topic}`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${config.token}`,
      },
      body: msg,
    });
  }
}
