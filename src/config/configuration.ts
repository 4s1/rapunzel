import { readFileSync } from 'fs';
import * as yaml from 'js-yaml';
import { join } from 'path';

export interface FuelPricesConfig {
  enabled: boolean;
  apiKey: string;
  groups: {
    name: string;
    stations: {
      name: string;
      id: string;
    }[];
  }[];
}

export interface StockPricesConfig {
  enabled: boolean;
  url: string;
  selector: {
    last: string;
    high: string;
    low: string;
  };
}

export interface NtfyConfig {
  enabled: boolean;
  url: string;
  token: string;
}

interface Config {
  fuelPrices: FuelPricesConfig;
  stockPrices: StockPricesConfig;
  ntfy: NtfyConfig;
}

const YAML_CONFIG_FILENAME = 'config.yaml';

export default () => {
  const config = yaml.load(
    readFileSync(
      join(__dirname, '..', '..', 'data', YAML_CONFIG_FILENAME),
      'utf8',
    ),
  ) as Config;

  //   if (config.http.port < 1024 || config.http.port > 49151) {
  //     throw new Error('HTTP port must be between 1024 and 49151');
  //   }

  return config;
};
